\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{common/styles/styly}[2021/05/17 v1.1 Overleaf Software Manual Template]
% sample software documentation format

% set default smaller margins, separated paragraphs, bigger line spacing
% (fullpage used as fallback if geometry package removed from main preamble)
\RequirePackage{fullpage,parskip}
\linespread{1.1}

% use Roboto as the main font
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[sfdefault]{roboto}
\RequirePackage[scaled=0.95]{roboto-mono}
\RequirePackage{sfmath}

% load the base class with default options
\LoadClass[11pt,a4paper]{article}



\AtEndPreamble{%
% set some hyperref options if it's loaded
% leave loading it to the user so they can get the order right
\@ifpackageloaded{hyperref}{%
\RequirePackage{xcolor}
\providecolor{manuallinkcolor}{HTML}{004a0e}
\hypersetup{colorlinks,allcolors=manuallinkcolor}%
}{}%
% let minted's listing captions use the correct font
\@ifpackageloaded{minted}{\let\@float@c@listing\@caption}{}
}
